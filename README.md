# spanish-holidays-remind 🇪🇸

Spanish Holidays in the REMIND Calendar format

NOTE: The README is not an exhaustive list

## National Holidays
* New Years
* Epiphany
* Easter
* Good Friday
* Ash Wednesday
* Palm Sunday
* Ascension Day
* Pentecost
* Labour Day
* Assumption of Mary
* National Day
* All Saints' Day
* Constitution Day
* Immaculate Conception
* Christmas Day

## Local/Regional Holidays
* Andalusia Day
* Valencian Community Day
* National Day of Catalonia
* Basque National Day
* Community of Madrid
* Madrid Community Day
* Galicia Day
* Canary Islands Day
* Balearic Islands Day
* St Vincent Ferrer Day
* Asturias Day
* Navarra Day
* Day of Cantabrian Constitution